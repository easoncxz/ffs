#!/usr/bin/env ruby
# Xuzong Chen - xche985

def rls(real_dir)
  Dir.entries(real_dir).select {|e| !e.start_with? '.'}
end

