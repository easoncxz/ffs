#!/usr/bin/env ruby
# Xuzong Chen - xche985

def init_ffs_root(real_dir)
  require_relative "DirTreeNode"
  require_relative "rls"
  root = DirTreeNode.new ""
  if !Dir.exist? real_dir
    Dir.mkdir real_dir
  end
  rls(real_dir).each do |l|
    root.new_leaf l
  end
  return root
end
