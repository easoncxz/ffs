#!/usr/bin/env ruby
# Xuzong Chen - xche985

require 'test/unit'
require_relative 'DirTreeNode'

class TestInit < Test::Unit::TestCase

  @root_sure

  @@REAL_DIR = "A2dir"

  require_relative "rls"
  def _rls
    rls(@@REAL_DIR)
  end

  require_relative "init_ffs_root"

  def setup
    if !Dir.exist? @@REAL_DIR
      Dir.mkdir @@REAL_DIR
    end
    @root_sure = DirTreeNode.new ""
    @root_sure.new_leaf "rrrr-dd-f0"
    @root_sure.new_leaf "rrrr-dd-f1"
    @root_sure.new_leaf "rrrr-dd-aaaa-g0"
    @root_sure.new_leaf "rrrr-dd-aaaa-g1"
    @root_sure.new_leaf "home-eason"
    assert_equal(5, _rls.size)
  end

  def teardown
    if Dir.exist? @@REAL_DIR
      if Dir.entries(@@REAL_DIR).size > 2
        Dir.entries(@@REAL_DIR).select do |f|
          if f.end_with? '/' then throw "Dirs nested too deep!" end
          !f.start_with? '.'
        end.each do |f|
          File.delete "#{@@REAL_DIR}/#{f}"
        end
      end
      Dir.delete @@REAL_DIR
    end
  end

  def test_init_from_existing
    root = init_ffs_root(@@REAL_DIR)
    assert_equal(2, root.children.size)
    rrrr = root.lookup_descendant "rrrr"
    assert(rrrr)
    dd = rrrr.lookup_descendant "dd"
    assert(dd)
    f0 = dd.lookup_descendant "f0"
    assert(f0)
    f1 = dd.lookup_descendant "f1"
    assert(f1)
    aaaa = dd.lookup_descendant "aaaa"
    assert(aaaa)
    g0 = aaaa.lookup_descendant "g0"
    assert(g0)
    g1 = aaaa.lookup_descendant "g1"
    assert(g1)

    assert_equal("-", root.ffs_abs_path)
  end

  def test_init_from_scratch
    teardown
    root = init_ffs_root @@REAL_DIR
    assert_equal("-", root.ffs_abs_path)
  end

end
