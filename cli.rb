#!/usr/bin/env ruby
# Xuzong Chen - xche985

def run
  require_relative "process"
  require_relative "init_ffs_root"
  context = init_ffs_root("A2dir")
  while true
    begin
      print "ffs> "
      line_with_sep = gets
      if line_with_sep
        if !STDIN.tty?
          print line_with_sep
        end
        context = process(line_with_sep.chomp, context)
      else
        puts
        puts "Goodbye"
        exit
      end
    rescue Interrupt
      puts
      next
    end
  end
end

if __FILE__ == $0
  run
end
