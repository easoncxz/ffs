
require 'test/unit'

class MyTestCase < Test::Unit::TestCase

  @fixture

  def setup
    @fixture = 0
  end

  def test_one
    assert(false,  "at test_one, the fixture is: #{@fixture += 1}")
  end

  def test_two
    assert(false,  "at test_two, the fixture is: #{@fixture += 1}")
  end

  def test_three
    assert(false,  "at test_three, the fixture is: #{@fixture += 1}")
  end

  def teardown
    puts "at teardown, fixture is: #{@fixture}"
  end

end
