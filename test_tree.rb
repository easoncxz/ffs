#!/usr/bin/env ruby
# Xuzong Chen - xche985

require 'test/unit'
require_relative 'DirTreeNode'

class TestTree < Test::Unit::TestCase

  @tree
  @@REAL_DIR = "A2dir"

  require_relative "rls"

  def _rls
    rls(@@REAL_DIR)
  end

  def setup
    if !Dir.exist? @@REAL_DIR
      Dir.mkdir @@REAL_DIR
    else
      `mv A2dir A2dir-persist`
      Dir.mkdir @@REAL_DIR
    end
    @tree = DirTreeNode.new ""
  end

  def teardown
    if Dir.exist? @@REAL_DIR
      if Dir.entries(@@REAL_DIR).size > 2
        Dir.entries(@@REAL_DIR).select do |f|
          if f.end_with? '/' then throw "Dirs nested too deep!" end
          !f.start_with? '.'
        end.each do |f|
          File.delete "#{@@REAL_DIR}/#{f}"
        end
      end
      Dir.delete @@REAL_DIR
    end
    if Dir.exist? "A2dir-persist"
      `mv A2dir-persist A2dir`
    end
  end

  def test_create_immediate_child
    leaf = @tree.new_leaf "f1"
    assert_equal(1, @tree.children.size)
    assert(@tree.children.include? leaf)
    assert_equal(@tree, leaf.parent)
    assert_equal(1, _rls.size)
    assert_equal("-f1", _rls[0])
  end

  def test_delete_immediate_child
    leaf = @tree.new_leaf "f1"
    assert_equal(1, _rls.size)
    leaf.delete_recursively

    assert_equal(0, @tree.children.size)
    f1 = @tree.lookup_descendant "f1"
    assert(!f1)
    assert_equal(0, _rls.size)
  end

  def test_create_nested_child
    leaf1 = @tree.new_leaf "dd-f0"
    leaf2 = @tree.new_leaf "dd-f1"
    assert_equal(2, _rls.size)

    assert_equal(1, @tree.children.size)
    dd = @tree.children[0]
    assert_equal("dd", dd.name)

    assert_equal(2, dd.children.size)
    assert_equal(dd, leaf1.parent)
    assert_equal(dd, leaf2.parent)

    assert_equal(2, _rls.size)
    assert_equal(
      ["-dd-f0", "-dd-f1"].sort,
      _rls.sort
    )
  end

  def test_delete_nested_child
    assert_equal(0, _rls.size)
    assert(!@tree.is_leaf?)

    @tree.new_leaf "-fileA"
    @tree.new_leaf "-rrrr-dd-aaaa-file0"
    @tree.new_leaf "-rrrr-dd-aaaa-file1"
    @tree.new_leaf "-rrrr-dd-file0"
    @tree.new_leaf "-rrrr-dd-file1"
    @tree.new_leaf "-rrrr-file1"
    @tree.new_leaf "-xx-file0"
    @tree.new_leaf "-xx-file1"
    @tree.new_leaf "-xx-o-aaa-xx-file0"
    @tree.new_leaf "-xx-o-aaa-xx-file1"
    @tree.new_leaf "-xx-o-file0"
    @tree.new_leaf "-xx-o-file1"
    assert_equal(3, @tree.children.size)
    assert_equal(12, _rls.size)
    assert(!@tree.is_leaf?)

    rrrr = @tree.lookup_descendant "rrrr"
    assert(!rrrr.is_leaf?)
    assert_equal(@tree, rrrr.parent)
    assert_equal(2, rrrr.children.size)

    dd = rrrr.lookup_descendant "dd"
    assert(!dd.is_leaf?)
    assert_equal(rrrr, dd.parent)
    assert_equal(3, dd.children.size)

    aaaa = dd.lookup_descendant "aaaa"
    assert(!aaaa.is_leaf?)
    assert_equal(dd, aaaa.parent)
    assert_equal(2, aaaa.children.size)

    file0 = aaaa.lookup_descendant "file0"
    file1 = aaaa.lookup_descendant "file1"
    assert(file0.is_leaf?)
    assert(file1.is_leaf?)
    assert_equal(aaaa, file0.parent)
    assert_equal(aaaa, file1.parent)

    aaaa.delete_recursively
    assert_equal(2, dd.children.size)
    assert_equal(10, _rls.size, _rls.join("\n"))

    rrrr.delete_recursively
    assert_equal(2, @tree.children.size)
    assert_equal(7, _rls.size, _rls.join("\n"))

    @tree.delete_recursively
    assert_equal(0, @tree.children.size)
    assert_equal(0, _rls.size)
  end

  def test_lookup
    leaf = @tree.new_leaf "dd-aaa-f0"

    assert_equal(1, @tree.children.size)
    dd_sure = @tree.children[0]
    assert_equal("dd", dd_sure.name)
    dd = @tree.lookup_descendant "dd"
    assert_equal(dd_sure, dd)

    assert_equal(1, dd.children.size)
    aaa_sure = dd.children[0]
    assert_equal("aaa", aaa_sure.name)
    aaa = dd.lookup_descendant "aaa"
    assert_equal(aaa_sure, aaa)

    assert_equal(1, aaa.children.size)
    f0_sure = aaa.children[0]
    assert_equal("f0", f0_sure.name)
    f0 = aaa.lookup_descendant "f0"
    assert_equal(f0_sure, f0)

    f0_directly = @tree.lookup_descendant "dd-aaa-f0"
    assert_equal(f0, f0_directly)
  end

  def test_ffs_abs_path  # depends on above tests
    f0 = @tree.new_leaf "dd-f0"
    dd = @tree.lookup_descendant "dd"

    assert_equal("-", @tree.ffs_abs_path)
    assert_equal("-dd-", dd.ffs_abs_path)
    assert_equal("-dd-f0", f0.ffs_abs_path)
  end

  def test_small_methods
    f0 = @tree.new_leaf "dd-f0"
    dd = @tree.lookup_descendant "dd"
    assert(@tree.is_root?)
    assert(f0.is_leaf?)
    assert_equal(@tree, f0.root)
    assert_equal(@tree, dd.root)
    assert_equal(0, @tree.depth)
    assert_equal(1, dd.depth)
    assert_equal(2, f0.depth)
  end

  def test_create_delete_nested_from_nested
    root = @tree
    f0 = root.new_leaf "rrrr-dd-f0"
    f1 = root.new_leaf "rrrr-dd-f1"
    dd = f0.parent
    rrrr = dd.parent
    assert_equal(root, rrrr.parent)
    assert_equal(
      ["-rrrr-dd-f0", "-rrrr-dd-f1"].sort,
      _rls.sort
    )

    dd.new_leaf "aaaa-g1"
    aaaa = dd.lookup_descendant "aaaa"
    assert(aaaa)
    g1 = aaaa.lookup_descendant "g1"
    assert(g1)
    g1_from_root = root.lookup_descendant "rrrr-dd-aaaa-g1"
    assert_equal(g1, g1_from_root)
    assert_equal(
      ["-rrrr-dd-f0",
       "-rrrr-dd-f1",
       "-rrrr-dd-aaaa-g1"].sort,
      _rls.sort
    )

    aaaa.delete_recursively
    assert_equal(2, dd.children.size)
    assert_equal(
      ["-rrrr-dd-f0", "-rrrr-dd-f1"].sort,
      _rls.sort
    )
    aaaa = aaaa.parent.lookup_descendant aaaa.name
    assert(!aaaa)
  end

  def test_ops_from_root
    root = @tree
    f0 = root.new_leaf "rr-dd-f0"
    dd = f0.parent
    rr = dd.parent
    assert_equal(root, rr.parent)

    fx = dd.new_leaf "-rr-a-fx"
    a = fx.parent
    assert_equal(rr, a.parent)
    assert_equal(2, rr.children.size)
    assert_equal(
      ["dd", "a"].sort,
      rr.children.map do |c|
        c.name
      end.sort
    )

    fx_from_dd = dd.lookup_descendant "-rr-a-fx"
    assert_equal(fx, fx_from_dd)
  end

  def test_root_parent
    assert(!@tree.parent)
  end

  def test_lookup_root
    root = @tree.lookup_descendant '-'
    assert_equal(@tree, root)
  end

  def test_ancestors
    assert_equal([], @tree.ancestors)
    leaf = @tree.new_leaf "-aa-bb-cc-dd-ee-file0"
    assert_equal("file0", leaf.name)
    assert_equal(
      ["", "aa", "bb", "cc", "dd", "ee"],
      leaf.ancestors.map do |n|
        n.name
      end.sort
    )
  end

end
