#!/usr/bin/env ruby
# Xuzong Chen - xche985

def process line, context
  command, args = line.split(' ', limit=2)
  if command && command != ""
    case command
    when "pwd"
      puts context.ffs_abs_path
    when "cd"
      if !args || args == ''
        context = context.root
      elsif args == '..'
        context = context.parent || context
      else
        target = context.lookup_descendant(args)
        if !target
          puts "Sorry, bad target directory. CWD not changed."
        elsif target.is_leaf?
          puts "Sorry, that's a file, not a directory"
        else
          context = target
        end
      end
    when "ls"
      if !args || args == ''
        temp = context
      else
        temp = context.lookup_descendant args
      end
      if temp
        temp.children.sort.each do |c|
          print (c.is_leaf? ? 'f: ' : 'd: ')
          puts c.name
        end
      else
        puts "Sorry, no such directory."
      end
    when "rls"
      print `ls -l A2dir`
    when "tree"
      if !args || args == ''
        target_context = context
      else
        target_context = context.lookup_descendant args
      end
      if !target_context
        puts "Sorry, bad target directory name"
      else
        require_relative "tree_drawer"
        tree_drawer target_context
      end
    when "clear"
      context.root.delete_recursively
    when "create"
      if !args || args == ''
        puts "Usage: create file_path"
      else
        target = context.lookup_descendant(args)
        if !target
          context.new_leaf args
        else
          puts "File #{target.name} already exists. Use 'add' to append text instead."
        end
      end
    when "add"
      rel_ffs_path, text = args.split(' ', 2)
      if !text || text == ''
        puts 'Usage: add file_path raw_text'
      else
        target = context.lookup_descendant(rel_ffs_path)
        if target
          real_path = 'A2dir/' + target.ffs_abs_path
          File.open(real_path, 'w') do |f|
            f << text << "\n"
          end
        else
          puts "File #{args} doesn't exist. Use 'create' to create it, then try again."
        end
      end
    when "cat"
      target = context.lookup_descendant(args)
      if target
        print File.read('A2dir/' + target.ffs_abs_path)
      else
        puts "File #{args} doesn't exist."
      end
    when "delete"
      if !args || args == ''
        puts "Usage: delete file_path"
      else
        target = context.lookup_descendant(args)
        if !target
          puts "No such file as '#{args}'"
        elsif !target.is_leaf?
          puts "#{target.name} is a directory. Use dd instead."
        else
          target.delete_recursively
        end
      end
    when "dd"
      if !args || args == ''
        puts "Usage: dd dir_path"
      else
        target = context.lookup_descendant(args)
        if !target
          puts "No such directory as '#{args}'."
        elsif target.is_leaf?
          puts "#{target.name} is a file. Use delete instead."
        elsif context.ancestors.include? target
          puts "Cannot delete a directory directly or indirectly containing the CWD."
        else
          target.delete_recursively
        end
      end
    when "quit"
      puts "Goodbye"
      exit
    when "#"
      # Do nothing
    else
      puts "Sorry, I can't recognize the command '#{command}'"
    end
  end
  return context
end
