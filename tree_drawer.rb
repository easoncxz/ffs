#!/usr/bin/env ruby
# Xuzong Chen - xche985

def tree_drawer(context, indent_level=0)
  indent = " " * 4
  if context
    if context.is_leaf?
      puts indent * indent_level + context.name
    else
      dir_full_ffs_path = (context.ancestors.map do |n|
        n.name
      end + [context.name + '-']).join('-')

      puts indent * indent_level + dir_full_ffs_path
      puts indent * indent_level + "=" * dir_full_ffs_path.length
      context.children.sort.each do |c|
        if c.is_leaf?
          tree_drawer(c, indent_level)
        else
          tree_drawer(c, indent_level + 1)
        end
      end
    end
  else
    puts "You're in some directory not being recognized."
  end
end
