#!/usr/bin/env ruby
# Xuzong Chen - xche985

# Represents a FFS directory or file. This performs real-fs-file operations in
# the real-fs folder.
class DirTreeNode

  # Name of the real directory within which FFS operates
  @@REAL_DIR = "A2dir"
  
  def initialize(name, is_leaf=false)
    @name = name  # Simple component name; will be "" for root
    @parent = nil  # A DirTreeNode instance; will be nil for root
    @children = []  # List of DirTreeNode instances
    @is_leaf = is_leaf

    # TODO: create corresponding real-fs file
  end

  attr_accessor :name, :parent, :children

  def is_leaf?
    return @is_leaf
  end

  def is_root?
    return !@parent  
  end

  def root
    if is_root?
      self
    else
      @parent.root
    end
  end

  def depth
    if is_root? then 0 else (1 + @parent.depth) end
  end

  # Returns the current absolute path of this DirTreeNode, starting from "-".
  def ffs_abs_path
    if is_root?
      '-'
    else
      @parent.ffs_abs_path + @name + (if is_leaf? then "" else "-" end)
    end
  end

  # Returns DirTreeNode of ffs_file_path in reference to this DirTreeNode
  def lookup_descendant(ffs_file_path)
    if ffs_file_path.start_with? '-'
      return root.lookup_descendant ffs_file_path[1..-1]
    else
      first_component, rest_of_path = ffs_file_path.split('-', 2)
      if first_component
        child = @children.find do |child|
          child.name == first_component
        end
      else
        child = root
      end
      if !child
        return nil
      elsif !rest_of_path || rest_of_path == ''
        return child
      else
        return child.lookup_descendant rest_of_path
      end
    end
  end

  def ancestors
    if is_root?
      return []
    else
      return @parent.ancestors + [@parent]
    end
  end

  # Creates and returns a new DirTreeNode that represents a ffs file that
  # resides within this DirTreeNode at the given relative path. All necessary
  # nodes in between will be created implicitly.
  def new_leaf(ffs_file_path)
    if ffs_file_path.start_with? '-'
      return root.new_leaf ffs_file_path[1..-1]
    elsif !ffs_file_path.include? '-'
      new_node = DirTreeNode.new ffs_file_path, is_leaf=true
      new_node.parent = self
      @children << new_node

      # real-fs operation here
      File.open("#{@@REAL_DIR}/#{new_node.ffs_abs_path}", 'w').close

      return new_node
    else
      first_component, rest_of_path = ffs_file_path.split('-', 2)
      if !rest_of_path
        throw "this piece of code should never be reached"
      elsif rest_of_path == ''
        throw "someone tried to create a directory directly"
      end
      child = lookup_descendant first_component
      if !child
        child = DirTreeNode.new first_component
        child.parent = self
        @children << child
      end
      return child.new_leaf rest_of_path
    end
  end

  # Delete all descendant DirTreeNodes of this one. The corresponding real-fs
  # file of each leaf will be deleted. Finally the corresponding real-fs file of
  # this DirTreeNode will be deleted.
  def delete_recursively
    if is_root?
      @children[0..-1].each do |c|
        c.delete_recursively
      end
    elsif is_leaf?
      File.delete "#{@@REAL_DIR}/#{ffs_abs_path}"
      @parent.children.delete self
    else
      @children[0..-1].each do |c|
        c.delete_recursively
      end
      @parent.children.delete self
    end
  end

  include Comparable

  # Directories get sorted after files. dir0 > file0
  def <=>(other)
    if self.is_leaf? && !other.is_leaf?
      -1
    elsif !self.is_leaf? && other.is_leaf?
      1
    else
      self.name <=> other.name
    end
  end

end
